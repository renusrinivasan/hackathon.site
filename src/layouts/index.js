import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/Header.js'

import './index.css'

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="Turo Hackathon 💻"
      link={[{'rel': 'icon', 'href': 'https://resources.turo.com/resources/img/favicon.png', 'type': 'image/x-icon'}]}
      meta={[
        { name: 'description', content: 'Hackathon at Turo' },
        { name: 'keywords', content: 'turo, hackathon' },
      ]}
    />
    <Header />
    <div className="background-pattern">
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          minHeight: '92vh',
          padding: '1.5rem',
        }}
      >
        {children()}
      </div>
    </div>
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
