import Link from 'gatsby-link'
import React from 'react'

const IndexPage = () => (
  <div style={{
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: '20vh',
  }}>
    <p>Fall 2017</p>
    <h5 style={{fontSize: 70}}>
      HACKATHON
    </h5>
    <p style={{fontSize: 36, marginBottom: '3rem'}}>
      November 8-9
    </p>
    <p>
      <i>San Francisco</i> | <i>Cambridge</i>
    </p>
  </div>
)

export default IndexPage
