import React from 'react'
import Link from 'gatsby-link'

import TuroLogo from './TuroLogo.js';

const linkStyle = {
  fontSize: 15,
  marginLeft: 20,
  textDecoration: 'none',
};

const Header = () => (
  <div className="background-pattern">
    <div
      style={{
        margin: '0 auto',
        padding: '1rem',
        display: 'flex',
        justifyContent: 'space-between'
      }}
    >
      <TuroLogo linkTo="/" />
      <div>
        <Link style={linkStyle} to="page-2">Editions</Link>
        <a style={linkStyle} href="https://team-turo.atlassian.net/wiki/spaces/EN/pages/20978945/Culture+Corps">Culture corps</a>
        <Link style={linkStyle} to="page-4">Contact</Link>
      </div>
    </div>
  </div>
);

export default Header;
