import React from 'react'
import Link from 'gatsby-link'

import logo from '../styles/images/logo_small.v7@2x.png'

const TuroLogo = ({linkTo = undefined}) => {
  const logoImage = <img src={logo} alt="Turo" style={{width: 66, height: 24}} />;

  return linkTo
    ? <Link to={linkTo} style={{textDecoration: 'none'}}>
        {logoImage}
      </Link>
    : logoImage;
};

export default TuroLogo;
